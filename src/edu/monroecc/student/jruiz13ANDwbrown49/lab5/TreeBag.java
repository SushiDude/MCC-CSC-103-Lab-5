// File: IntTreeBag.java from the package edu.colorado.collections

// The implementation of most methods in this file is left as a student
// exercise from Section 9.5 of "Data Structures and Other Objects Using Java"

// Check with your instructor to see whether you should put this class in
// a package. At the moment, it is declared as part of edu.colorado.collections:
package edu.monroecc.student.jruiz13ANDwbrown49.lab5;

import edu.colorado.nodes.BTNode;

/** This class is a homework assignment; A <CODE>TreeBag</CODE> is a collection
 * of Objects.
 *
 * <b>Outline of Java Source Code for this class:</b>
 * <A HREF="../../../../edu/colorado/collections/IntTreeBag.java"> http://
 * www.cs.colorado.edu/~main/edu/colorado/collections/IntTreeBag.java </A>
 *
 * <b>Note:</b> This file contains only blank implementations ("stubs") because
 * this is a Programming Project for my students.
 *
 * @version Jan 24, 1999
 *
 * @see BTNode **/
/* XXX: BTNode is a raw type. References to generic type BTNode<E> should be
 * parameterized. However, the specification mandates that BTNode must store an
 * Object. */
/* XXX: Comparable is a raw type. References to generic type Comparable<T>
 * should be parameterized. However, the specification mandates that the remove,
 * add, and retrieve methods must accept a Comparable. */
public class TreeBag
{
	// Invariant of the TreeBag class:
	// 1. The elements in the bag are stored in a binary search tree.
	// 2. The instance variable root is a reference to the root of the
	// binary search tree (or null for an empty tree).
	private BTNode root;

	/** Insert a new element into this bag. <b>Postcondition:</b> A new copy of
	 * the element has been added to this bag.
	 *
	 * @param element the new element that is being inserted.
	 * @exception OutOfMemoryError Indicates insufficient memory a new
	 *            BTNode. **/
	public void add(final Comparable element)
	{
		root = addNode(element, root);
	}

	private BTNode addNode(final Comparable x, BTNode p)
	{
		if (p == null)
			// create the node - base case
			p = new BTNode(x, null, null);
		else if (x.compareTo(p.getData()) < 0)
			p.setLeft(addNode(x, p.getLeft()));
		else if (x.compareTo(p.getData()) > 0)
			p.setRight(addNode(x, p.getRight()));
		else // keys are equal - replace with new data
			p.setData(x);
		return p;
	}

	/** Prints all objects in ascending order to the standard output stream. */
	public void display()
	{
		if (root != null)
			root.inorderPrint();
	}

	/** Remove one copy of a specified element from this bag.
	 *
	 * @param target the element to remove from the bag <b>Postcondition:</b> If
	 *        <CODE>target</CODE> was found in the bag, then one copy of
	 *        <CODE>target</CODE> has been removed and the method returns true.
	 *        Otherwise the bag remains unchanged and the method returns false.
	 * @return a boolean representing if the target was in the tree. **/
	public boolean remove(final Comparable target)
	{
		if (retrieve(target) != null)
		{
			root = removeNode(root, target);
			return true;
		} else
			return false;
	}

	private BTNode removeNode(BTNode p, final Comparable target)
	{
		if (p == null) // tree is empty, target is not found
			return p;
		if (target.compareTo(p.getData()) < 0)
			p.setLeft(removeNode(p.getLeft(), target));
		else if (target.compareTo(p.getData()) > 0)
			p.setRight(removeNode(p.getRight(), target));
		// found the node to be removed
		// if the node has two children
		else if (p.getLeft() != null && p.getRight() != null)
		{
			// get the data in the right most node in the left subtree
			p.setData(p.getLeft().getRightmostData());
			// delete the right most data in the left subtree
			p.setLeft(p.getLeft().removeRightmost());
		} else if (p.getLeft() == null) // only right child
			p = p.getRight();
		else // only left child
			p = p.getLeft();
		return p;
	}

	/** Retrieve one copy of a specified element from this bag.
	 *
	 * @param element the element to retrieve from the bag.
	 * @return an Object representing the retrieved element. */
	public Object retrieve(final Comparable element)
	{
		final BTNode bTNode = retrieveNode(root, element);
		return bTNode == null ? null : bTNode.getData();
	}

	private BTNode retrieveNode(final BTNode p, final Comparable searchKey)
	{
		if (p == null)
			return null;// base case: not there
		else if (searchKey.compareTo(p.getData()) == 0)
			return p;// base case: found
		else if (searchKey.compareTo(p.getData()) < 0)
			return retrieveNode(p.getLeft(), searchKey);
		else
			return retrieveNode(p.getRight(), searchKey);
	}
}
