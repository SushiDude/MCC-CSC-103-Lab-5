package edu.monroecc.student.jruiz13ANDwbrown49.lab5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

/** The class {@code Company} implements methods for storing {@code Employee}s
 * in a {@code TreeBag}.
 *
 * @author Fig
 * @author Wyatt J. Brown
 * @see Employee
 * @see TreeBag */
public class Company
{
	private final TreeBag treeBag = new TreeBag();

	/** Creates a menu with the following options:
	 * <ol>
	 * <li>Read from a file the list of employees and store each employee in a
	 * Binary Search Tree, based on the employee id (unique number)</li>
	 * <li>Add an employee to the tree</li>
	 * <li>Remove an employee from the tree</li>
	 * <li>Retrieve an employee</li>
	 * <li>Update an employee</li>
	 * <li>Display the firm's employees in ascending order based on the employee
	 * id</li>
	 * <li>Quit</li>
	 * </ol>
	 *
	 * @param option a String parameter that has the option to select from the
	 *        menu, along with the other numbers that are related to that
	 *        option.
	 * @throws IOException if the input file can not be read. */
	public void menu(final String option) throws IOException
	{
		final String[] options = option.replaceAll("-|–", " ").split("\\s+");
		switch (options[0])
		{
		case "1":/* Read from a file the list of employees and store each
					 * employee in a Binary Search Tree, based on the employee
					 * id (unique number). */
			final BufferedReader bufferedReader =
					new BufferedReader(new FileReader(options[1]));
			for (String line = bufferedReader.readLine(); line != null; line =
					bufferedReader.readLine())// Read file line by line.
			{
				final String arguments[] = line.split("\\s+");
				treeBag.add(new Employee(Integer.parseInt(arguments[0]),
						arguments[2], arguments[1],
						Double.parseDouble(arguments[3])));
			}
			bufferedReader.close();// Close the file.
			break;
		case "2":// Add an employee to the tree.
			treeBag.add(new Employee(Integer.parseInt(options[1]), options[3],
					options[2], Double.parseDouble(options[4])));
			break;
		case "3":// Remove an employee from the tree.
			treeBag.remove(new Employee(Integer.parseInt(options[1])));
			break;
		case "4":// Retrieve an employee.
			final Employee retrieveEmployee = (Employee) treeBag
					.retrieve(new Employee(Integer.parseInt(options[1])));
			if (retrieveEmployee != null)
				System.out.println(retrieveEmployee.toString());
			break;
		case "5":// Update an employee.
			final Employee updateEmployee = (Employee) treeBag
					.retrieve(new Employee(Integer.parseInt(options[1])));
			if (updateEmployee != null)
				updateEmployee.setSalary(Double.parseDouble(options[2]));
			break;
		case "6":/* Display the firm's employees in ascending order based on the
					 * employee id. */
			treeBag.display();
			break;
		case "7":// Quit.
			/* Create an output file called outEmployees.txt which has all the
			 * nodes that are in the tree. */
			System.setOut(new PrintStream(new File("outEmployees.txt")));
			treeBag.display();

			System.exit(0);// Exit cleanly.
			break;
		}
	}
}
