package edu.monroecc.student.jruiz13ANDwbrown49.lab5;

/** The class {@code Employee} represents an employee that has a unique
 * identification number, last and first name, and salary.
 *
 * @author Fig
 * @author Wyatt J. Brown */
/* XXX: Comparable is a raw type. References to generic type Comparable<T>
 * should be parameterized. However, the specification mandates that the
 * compareTo method must accept an Object and therefore not an Employee. */
public class Employee implements Cloneable, Comparable
{
	private final int acctID;
	private String firstName;
	private String lastName;
	private double salary;

	/** Constructs an employee that has a given unique identification number.
	 *
	 * @param id an int representing the unique identification number for the
	 *        given employee. */
	public Employee(final int id)
	{
		acctID = id;
	}

	/** Constructs an employee that has a given unique identification number,
	 * last and first name, and salary.
	 *
	 * @param id an int representing the unique identification number for the
	 *        given employee.
	 * @param l_name a String representing the last name of the given employee.
	 * @param f_name a String representing the first name of the given employee.
	 * @param amount a double representing the salary of the given employee. */
	public Employee(final int id, final String l_name, final String f_name,
			final double amount)
	{
		acctID = id;
		lastName = l_name;
		firstName = f_name;
		salary = amount;
	}

	@Override
	public int compareTo(final Object obj)
	{
		return acctID - ((Employee) obj).getId();
	}

	/** @return a String representing the first name of the given employee. */
	public String getFirstName()
	{
		return firstName;
	}

	/** @return an int representing a unique identifier for the given
	 *         employee. */
	public int getId()
	{
		return acctID;
	}

	/** @return a String representing the last name of the given employee. */
	public String getLastName()
	{
		return lastName;
	}

	/** @return a double representing the salary of the given employee. */
	public double getSalary()
	{
		return salary;
	}

	/** @param amount a double representing the salary of the given employee. */
	public void setSalary(final double amount)
	{
		salary = amount;
	}

	@Override
	public String toString()
	{
		return acctID + "\t" + firstName + "\t" + lastName + "\t" + salary;
	}

}
